package com.bci.dto;

import java.util.Collection;

public class UsuarioEntradaDTO {
    private String name;
    private String email;
    private String password;
    private String token;
    private Collection<TelefonoDTO> phones;

    public UsuarioEntradaDTO(String name, String email, String password, String token, Collection<TelefonoDTO> phones) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.token = token;
        this.phones = phones;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Collection<TelefonoDTO> getPhones() {
        return phones;
    }

    public void setPhones(Collection<TelefonoDTO> phones) {
        this.phones = phones;
    }
}
