package com.bci.dto;

import java.sql.Date;

public class RespuestaCreacion {

    String id, token, isactive;
    Date crated, modified, last_login;

    public RespuestaCreacion(String id, String token, String isactive, Date crated, Date modified, Date last_login) {
        this.id = id;
        this.token = token;
        this.isactive = isactive;
        this.crated = crated;
        this.modified = modified;
        this.last_login = last_login;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getIsactive() {
        return isactive;
    }

    public void setIsactive(String isactive) {
        this.isactive = isactive;
    }

    public Date getCrated() {
        return crated;
    }

    public void setCrated(Date crated) {
        this.crated = crated;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public Date getLast_login() {
        return last_login;
    }

    public void setLast_login(Date last_login) {
        this.last_login = last_login;
    }
}
