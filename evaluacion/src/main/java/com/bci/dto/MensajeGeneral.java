package com.bci.dto;

public class MensajeGeneral {

    private String mensaje;

    public MensajeGeneral(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
