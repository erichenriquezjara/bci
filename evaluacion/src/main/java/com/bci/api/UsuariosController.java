package com.bci.api;

import com.bci.dto.MensajeGeneral;
import com.bci.dto.RespuestaCreacion;
import com.bci.dto.UsuarioEntradaDTO;
import com.bci.entidades.Usuario;
import com.bci.servicios.UsuarioServ;
import com.bci.utilidades.Util;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@Api
public class UsuariosController {

    @Autowired
    UsuarioServ UsuarioServ;

    @Autowired
    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    HttpServletRequest request;

    @ApiOperation( value = "Saludo Simple", response = String.class)
    @RequestMapping(value = "/usuario/saludo",
            produces = { "application/json" },
            method = RequestMethod.GET)
    public ResponseEntity<String> saludar() {
        return new ResponseEntity<String>(
                "{ \"mensaje\" : \"Hola\"}",
                HttpStatus.OK
        );
    }

    @ApiOperation( value = "Devuelve una Lista de Usuarios creados.", response = Usuario[].class)
    @RequestMapping(value = "/usuario/consulta",
            produces = { "application/json" },
            method = RequestMethod.GET)
    public ResponseEntity<List<Usuario>> consultaUsuarios() {
        return new ResponseEntity<>(
            UsuarioServ.consultarUsuarios(),
            HttpStatus.OK
        );
    }

    @ApiOperation( value = "Intenta crear un usuario con los datos enviados.", response = RespuestaCreacion.class)
    @RequestMapping(
            value = "/usuario/crearUsuario",
            produces = { "application/json" },
            consumes = { "application/json" },
            method = RequestMethod.POST)
    public ResponseEntity<String> crearUsuario(@RequestBody UsuarioEntradaDTO usuario) {

        // Verificamos que cumpla con los requisitos de correo.
        if( !Util.validarCorreo( usuario.getEmail() ) ){
            try {
                return new ResponseEntity<String>(
                        mapper.writeValueAsString( new MensajeGeneral("El correo no tiene un formato Válido - abc@cde.fg") ),
                        HttpStatus.FORBIDDEN
                );
            } catch (JsonProcessingException e) {
                return new ResponseEntity<String>(
                        "{\"mensaje\" : \"" + e.getMessage() + "\" }",
                        HttpStatus.INTERNAL_SERVER_ERROR
                );
            }
        }

        // Verificamos que cumpla con los requisitos de Password.
        if( !Util.validarFormatoPassword( usuario.getPassword() ) ){
            try {
                return new ResponseEntity<String>(
                        mapper.writeValueAsString( new MensajeGeneral("El Password no tiene un formato Válido - Aaaaaa00") ),
                        HttpStatus.FORBIDDEN
                );
            } catch (JsonProcessingException e) {
                return new ResponseEntity<String>(
                        "{\"mensaje\" : \"" + e.getMessage() + "\" }",
                        HttpStatus.INTERNAL_SERVER_ERROR
                );
            }
        }

        // Consultamos si existe el Email.
        if( UsuarioServ.consultarUsuarioPorEmail( usuario.getEmail() ) ) {
            try {
                return new ResponseEntity<String>(
                    mapper.writeValueAsString( new MensajeGeneral("El correo ya registrado") ),
                    HttpStatus.CONFLICT
                );
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }

        // Si cumple con las verificaciones lo creamos.
        Usuario usuarioModel = UsuarioServ.crearUsuario( usuario );

        RespuestaCreacion resp = new RespuestaCreacion(
            usuarioModel.getId().toString(),
            usuarioModel.getToken(),
            usuarioModel.getIsactive().toString(),
            usuarioModel.getCreated(),
            usuarioModel.getModified(),
            usuarioModel.getLastLogin()
        );

        try {
            return new ResponseEntity<String>(
                mapper.writeValueAsString( resp ),
                HttpStatus.CREATED
            );
        } catch (JsonProcessingException e) {
            return new ResponseEntity<String>(
                "{\"mensaje\" : \"" + e.getMessage() + "\" }",
                HttpStatus.INTERNAL_SERVER_ERROR
            );
        }

    }

}
