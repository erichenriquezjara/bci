package com.bci.repositorios;

import com.bci.entidades.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface UsuarioRep extends JpaRepository<Usuario, UUID> {

    @Query("select count(u) from Usuario u where upper(u.email) = upper(:email)")
    Long consultarUsuarioPorEmail(
        @Param("email") String email
    );
}
