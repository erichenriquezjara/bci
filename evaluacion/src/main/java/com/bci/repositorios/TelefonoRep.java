package com.bci.repositorios;

import com.bci.entidades.Telefono;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TelefonoRep extends JpaRepository<Telefono, Integer> {
}
