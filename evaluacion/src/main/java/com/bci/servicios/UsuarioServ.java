package com.bci.servicios;

import com.bci.dto.UsuarioEntradaDTO;
import com.bci.entidades.Telefono;
import com.bci.entidades.Usuario;
import com.bci.repositorios.TelefonoRep;
import com.bci.repositorios.UsuarioRep;
import com.bci.utilidades.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;

@Service
public class UsuarioServ {

    @Autowired
    UsuarioRep usuarioRep;

    @Autowired
    TelefonoRep telefonoRep;

    public Usuario crearUsuario( UsuarioEntradaDTO usuarioEntrada ){

        Usuario usuario = new Usuario();

        Date ahora = Util.ahora();
        usuario.setName( usuarioEntrada.getName() );
        usuario.setEmail( usuarioEntrada.getEmail() );
        usuario.setPassword( usuarioEntrada.getPassword() );
        usuario.setToken( usuarioEntrada.getToken() );

        usuario.setCreated( ahora );
        usuario.setModified( ahora );
        usuario.setLastLogin( ahora );
        usuario.setIsactive( 1 );

        final Usuario usuarioFinal = usuarioRep.saveAndFlush( usuario );

        usuarioEntrada.getPhones().forEach(
            tel-> {
                telefonoRep.saveAndFlush( new Telefono( tel.getNumber(), tel.getCitycode(), tel.getContrycode(), usuarioFinal) );
            }
        );

        return usuario;
    }

    public boolean consultarUsuarioPorEmail(String email) {
        return this.usuarioRep.consultarUsuarioPorEmail(email) > 0L;
    }

    public List<Usuario> consultarUsuarios() {
        List<Usuario> resp = this.usuarioRep.findAll();
        resp.forEach(
            usu -> usu.getPhones().forEach(
                phone -> {
                    phone.setUsuarioByIdUsuario( null );
                }
            )
        );
        return resp;
    }
}
