package com.bci.entidades;


import javax.persistence.*;
import java.util.Objects;

@Entity
public class Telefono {

    private Integer id;
    private Integer number;
    private Integer citycode;
    private Integer contrycode;
    private Usuario usuarioByIdUsuario;

    @Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "number")
    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    @Basic
    @Column(name = "citycode")
    public Integer getCitycode() {
        return citycode;
    }

    public void setCitycode(Integer citycode) {
        this.citycode = citycode;
    }

    @Basic
    @Column(name = "countrycode")
    public Integer getContrycode() {
        return contrycode;
    }

    public void setContrycode(Integer countrycode) {
        this.contrycode = countrycode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Telefono telefono = (Telefono) o;
        return Objects.equals(id, telefono.id) &&
                Objects.equals(number, telefono.number) &&
                Objects.equals(citycode, telefono.citycode) &&
                Objects.equals(contrycode, telefono.contrycode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, number, citycode, contrycode);
    }

    @ManyToOne
    @JoinColumn(name = "id_usuario", referencedColumnName = "id", nullable = false)
    public Usuario getUsuarioByIdUsuario() {
        return usuarioByIdUsuario;
    }

    public void setUsuarioByIdUsuario(Usuario usuarioByIdUsuario) {
        this.usuarioByIdUsuario = usuarioByIdUsuario;
    }

    public Telefono(){}

    public Telefono(Integer number, Integer citycode, Integer countrycode, Usuario usuarioByIdUsuario) {
        this.number = number;
        this.citycode = citycode;
        this.contrycode = countrycode;
        this.usuarioByIdUsuario = usuarioByIdUsuario;
    }
}
