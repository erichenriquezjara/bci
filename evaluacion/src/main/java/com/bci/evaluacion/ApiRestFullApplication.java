package com.bci.evaluacion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication(scanBasePackages = {"com.bci.entidades", "com.bci.repositorios", "com.bci.servicios", "com.bci.api", "com.bci.swagger"})
@ComponentScan({"com.bci.entidades", "com.bci.repositorios", "com.bci.servicios", "com.bci.api", "com.bci.swagger"})
@EntityScan("com.bci.entidades")
@EnableJpaRepositories("com.bci.repositorios")
@EnableSwagger2
public class ApiRestFullApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiRestFullApplication.class, args);
	}

}
