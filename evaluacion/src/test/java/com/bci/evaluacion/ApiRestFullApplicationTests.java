package com.bci.evaluacion;

import com.bci.api.UsuariosController;
import com.bci.entidades.Usuario;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;

import java.net.URI;
import java.net.URISyntaxException;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApiRestFullApplicationTests {

	@LocalServerPort
	int puerto;

	@Autowired
	private UsuariosController usuariosController;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void contextLoads() {
	}

	@Test
	public void controllerLoads(){
		assertThat(usuariosController).isNotNull();
	}

	@Test
	public void crearUsuarioCorreoMalo() throws URISyntaxException {
		final String url = "http://localhost:" + puerto + "/usuario/crearUsuario";
		URI uri = new URI(url);
		Usuario usu = getUsuarioDePrueba();
		usu.setEmail("test.com");

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Usuario> request = new HttpEntity<>(usu, headers);

		ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);
		Assertions.assertEquals( HttpStatus.FORBIDDEN, result.getStatusCode() );
	}

	@Test
	public void crearContraseñaMala() throws URISyntaxException {
		final String url = "http://localhost:" + puerto + "/usuario/crearUsuario";
		URI uri = new URI(url);
		Usuario usu = getUsuarioDePrueba();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Usuario> request = new HttpEntity<>(usu, headers);

		ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);
		Assertions.assertEquals( HttpStatus.FORBIDDEN, result.getStatusCode() );
	}

	@Test
	public void crearUsuarioVálidoORepetido() throws URISyntaxException {
		final String url = "http://localhost:" + puerto + "/usuario/crearUsuario";
		URI uri = new URI(url);
		Usuario usu = getUsuarioDePrueba();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Usuario> request = new HttpEntity<>(usu, headers);

		ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);
		Assertions.assertNotEquals( HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode() );
	}

	public static Usuario getUsuarioDePrueba(){
		Usuario usu = new Usuario();
		usu.setName("Eric");
		usu.setEmail("eric@henriquez.cl");
		usu.setPassword("12345");
		return usu;
	}

}
